currentScript = document.currentScript;
postscriptum.plugin('ps-tests-mutations', (processor, options) => {
	const { io, logger } = postscriptum.util;
	let beforeDOM;
	let domDiff;
	let firstPass = true;

	processor.on('pagination-start', async function() {
		await io.loadScript("https://cdn.jsdelivr.net/gh/fiduswriter/diffDOM@5.0.4/browser/diffDOM.js");
		domDiff = new diffDOM.DiffDOM();
	});

	processor.on('contents-start', function () {
		if (firstPass) {
			beforeDOM = this.currentFragment.body.cloneNode(true);
			beforeDOM.normalize();
		}
	});

	function onEnd() {
		if (firstPass) {
			this.layoutContext.mutations.revert();
			const afterDOM = this.currentFragment.body;
			afterDOM.normalize();
			const diff = domDiff.diff(beforeDOM, afterDOM);
			if (diff.length) {
				logger.warn(JSON.stringify(diff, null, 2));
				debugger;
			}
			firstPass = false;
			return 'detect-overflow';
		}
	}

	processor.on('valid-break-point', onEnd);
	processor.on('no-break-point', onEnd);

	processor.on('contents-end', function () {
		firstPass = true;
	});

});