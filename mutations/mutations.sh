#!/bin/bash
if [ "$#" -ne 1 ]; then
    echo "Usage: mutations.sh [DIR]"
    exit 1;
fi

if [ ! -d "$1" ]; then
	echo "Invalid directory"
	exit 1;
fi

baseDir=$(dirname $(readlink -f "$BASH_SOURCE"))/..
cd "$1"
for input in $(find . \( -name '*.html' -o -name '*.htm' -o -name '*.xhtml' -o -name '*.xht' \)); do
	echo -n " - " && echo "${input}" | cut -c3-
	${baseDir}/../app/scripts/cli-dev -p "ps-tests-mutations:${baseDir}/mutations/ps-tests-mutations.js" -o ~/Bureau/output.pdf "${input}"
done