# postscriptum-tests

This repository contains the tests suites used by Postscriptum.

## Postscriptum

Postscriptum is a solution to produce PDF from HTML with the help of CSS Print.

Please refer to [__https://postscriptum.app__](https://postscriptum.app) for a global picture of the project.

## Issues

Please report any issues on the [*postscriptum-core* project](https://gitlab.com/postscriptum.app/core/-/issues).