const path = require('path');
const fs = require('fs');
const os = require('os');

const timeout = 300;

function outputBasepath(input) {
	return `${input.dirname}/${input.basename}`;
}

async function psCommand(inputPath, args) {
	const dirname = path.dirname(inputPath);
	const basename = path.basename(inputPath, path.extname(inputPath));
	const testArgsFile = `${dirname}/${basename}.cliargs`;
	let testArgs = [];
	if (fs.existsSync(testArgsFile)) testArgs = fs.readFileSync(testArgsFile, 'utf-8').trim().split(' ');
	return {
		executable: 'POSTSCRIPTUM_PATH' in process.env ? path.resolve(process.env['POSTSCRIPTUM_PATH']) : 'postscriptum',
		args: [
			"--log-level",
			"debug",
			/*"--chrome-path",
			"/opt/google/chrome-beta/chrome",*/
			//"-p",
			//`ps-tests-mutations:${__dirname}/mutations/ps-tests-mutations.js`,
			//"--chrome-port",
			//"9331",
			inputPath,
			...testArgs,
			...args
		],
		cwd: dirname,
		timeout: timeout * 1000
	};
}

module.exports = {
	"inputs": [
		"**/*.html",
		"**/*.xhtml",
		"**/*.xht"
	],
	"indexes": /index\.(html|xhtml|htm|xht)$/i,
	"processor": ({input, testOutput}) => psCommand(input, [ "-o", testOutput, "--timeout", timeout.toString() ]),
	"comparator": {
		"executable": "diff-pdf",
		"args": [
			"-s",
			"-m",
			"--output-diff={diffOutput}",
			"{fileA}",
			"{fileB}"
		],
		"timeout": timeout * 1000
	},
	"outputs": {
		"test": (input) => `${outputBasepath(input)}.test.pdf`,
		"approved": (input) => `${outputBasepath(input)}.approved.pdf`,
		"rejected": (input) => `${outputBasepath(input)}.rejected.pdf`,
		"diff": (input) => `${outputBasepath(input)}.diff.pdf`,
		"error": (input) => `${outputBasepath(input)}.error.txt`,
		"note": (input) => `${outputBasepath(input)}.note.txt`
	},
	"tools": {
		"viewers": {
			"psDev": {
				"label": "Postscriptum Dev",
				"command": ({path}) => psCommand(path, [ "-p", "ps-print-preview"])
			},
			"vivliostyleViewer": {
				"label": "Vivliostyle Viewer",
				"command": "vivliostyle preview -s A4 {path}"
			},
			"diff-pdf": {
				"label": "diff-pdf",
				"command": {
					"executable": "diff-pdf",
					"args": [
						"-m",
						"--view",
						"{checkedFile}",
						"{testFile}"
					]
				}
			}
		},
		"processors": {
			"prince": {
				"label": "Prince",
				"command": "prince {input} -o {testOutput}",
				"extension": ".pdf"
			},
			"pdfreactor": {
				"label": "PDFreactor",
				"command": "pdfreactor-cmd -i {input} -o {testOutput}",
				"extension": ".pdf"
			},
			"paged.js": {
				"label": "Paged.js",
				"command": "pagedjs-cli {input} -o {testOutput}",
				"extension": ".pdf"
			},
			"Vivliostyle": {
				"label": "Vivliostyle",
				"command": "vivliostyle build -s 4 {input} -o {testOutput}",
				"extension": ".pdf"
			}
		}
	},
	concurrency: Math.floor(os.cpus().length / 2)
}
